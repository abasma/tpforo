//imports
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getAuth,
  onAuthStateChanged,
  signOut,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import {
  getDatabase,
  ref,
  set,
  onValue,
  push,
  query,
  orderByChild,
  child,
  get,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

//firebase keys
const firebaseConfig = {
  apiKey: "AIzaSyDD41AsOVXm8RrJPJmQ-2IzrBjFhTf7Zqg",
  authDomain: "proyectoforo-b8436.firebaseapp.com",
  databaseURL: "https://proyectoforo-b8436-default-rtdb.firebaseio.com/",
  projectId: "proyectoforo-b8436",
  storageBucket: "proyectoforo-b8436.appspot.com",
  messagingSenderId: "743344245619",
  appId: "1:743344245619:web:ef82cfb32a9097b0b79306",
};
//firebase inicialization
const app = initializeApp(firebaseConfig);
//auth
const auth = getAuth();
//cheking current user
const user = auth.currentUser;
//database
const database = getDatabase();

var msgRef = document.getElementById("textChatId");
var chatRef = document.getElementById("msgChatId");

// Reference chat btn and eventlistener
var sendRef = document.getElementById("buttonChatId");
sendRef.addEventListener("click", enviarMensaje);

// Reference logout btn and eventlistener
var logOut = document.getElementById("logoutBtn");
logOut.addEventListener("click", loggingOut);

let mail;
let userGlobal;

onAuthStateChanged(auth, (user) => {
  if (user) {
    // Mail and UID from logged user
    const uid = user.uid;
    mail = user.email;
    console.log("Your UID is: " + uid);

    get(ref(database, `users/${uid}`))
      .then((snapshot) => {
        if (snapshot.exists()) {
          userGlobal = snapshot.val();
        } else {
          console.log("No data available");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  } else {
  }
});

function enviarMensaje() {
  let msg = msgRef.value;

  push(ref(database, "message/"), {
    msg: msg,
    email: userGlobal.Email,
    contry: userGlobal.Country,
    aka: userGlobal.Name
  });

  console.log("Message sent by: " + mail);
  console.log("The msg contains: " + msg);
  msg = "";
}

const queryMessages = query(ref(database, "message/"));

console.log(queryMessages);

onValue(queryMessages, (snapshot) => {
  chatRef.innerHTML = "";
  snapshot.forEach((childSnapshot) => {
    const message = childSnapshot.val();

    chatRef.innerHTML += `
          <p>${message.aka}: ${message.msg} <span style="font-size:0.8rem">| from ${message.contry}</span></p>
      `;
  });
});

function loggingOut() {
  const auth = getAuth();
  signOut(auth)
    .then(() => {
      window.location.href = "./Home.html";
    })
    .catch((error) => { });
}
