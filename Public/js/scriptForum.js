//imports
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged, signOut} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js'
import { getDatabase, ref, set, onValue, push, query, orderByChild, child, get} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js'

//firebase keys
const firebaseConfig = {
  apiKey: "AIzaSyDD41AsOVXm8RrJPJmQ-2IzrBjFhTf7Zqg",
  authDomain: "proyectoforo-b8436.firebaseapp.com",
  databaseURL: "https://proyectoforo-b8436-default-rtdb.firebaseio.com/",
  projectId: "proyectoforo-b8436",
  storageBucket: "proyectoforo-b8436.appspot.com",
  messagingSenderId: "743344245619",
  appId: "1:743344245619:web:ef82cfb32a9097b0b79306",
};
//firebase inicialization 
const app = initializeApp(firebaseConfig);
//database
const database = getDatabase();

var msgRef = document.getElementById("textChatId");
var chatRef = document.getElementById("msgChatId");


const queryMessages = query(ref(database, 'message/'));
console.log(queryMessages);

onValue(queryMessages, (snapshot) => {
    chatRef.innerHTML = '';
    snapshot.forEach((childSnapshot) => {
      const message = childSnapshot.val();
      
      chatRef.innerHTML += `
        <p>${message.aka}: ${message.msg} <span style="font-size:0.8rem">| from ${message.contry}</span></p>
      `;
    });
});

